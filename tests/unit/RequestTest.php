<?php


class RequestTest extends \PHPUnit_Framework_TestCase
{

    // tests
    public function testCreate()
    {
        $request = TestHelper::createRequest('http://example.com/foo?bar=bar');
        $this->assertInstanceOf('\core\base\routing\Request', $request);
        $this->assertEquals('/foo', $request->getPathInfo());
        $this->assertTrue($request->isGet());
        $this->assertEquals('example.com', $request->getHost());
        $this->assertEquals('http://example.com', $request->getHttpHost());
        $this->assertFalse($request->isSecure());
        $this->assertFalse($request->isPost());
        $this->assertFalse($request->isAjax());

        $request = TestHelper::createRequest('https://example.com/controller/action/10?foo=bar', 'POST', ['QUERY_STRING' => 'foo=bar', 'HTTP_X_REQUESTED_WITH' => 'XMLHttpRequest']);
        $this->assertEquals('/controller/action/10', $request->getPathInfo());
        $this->assertEquals('foo=bar', $request->getQueryString());
        $this->assertTrue($request->isSecure());
        $this->assertTrue($request->isPost());
        $this->assertTrue($request->isAjax());

    }
}