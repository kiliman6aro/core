<?php

use core\base\routing\Manager;

class ManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testHandleRequest()
    {
        $request = \TestHelper::createRequest('http://example.loc/blog/10');

        $manager = new Manager();
        $manager->addRules([
            '/blog' => 'blog/index',
            '/blog/{id:any}' => 'blog/view',
            '/blog/{alias:s}/{id:d}' => 'blog/category/view',
        ]);

        $this->assertInstanceOf('\core\base\routing\Manager', $manager);
        $result = $manager->handleRequest($request);
        $this->assertEquals('blog/view', $result[0]);
        $this->assertEquals(1, count($result[1]));
        $this->assertArrayHasKey('id', $result[1]);
        $this->assertEquals(10, $result[1]['id']);

        $request = \TestHelper::createRequest('http://example.loc/blog/news/10');
        $result = $manager->handleRequest($request);
        $this->assertEquals('blog/category/view', $result[0]);
        $this->assertEquals(2, count($result[1]));
        $this->assertArrayHasKey('alias', $result[1]);
        $this->assertArrayHasKey('id', $result[1]);
        $this->assertEquals(10, $result[1]['id']);
        $this->assertEquals('news', $result[1]['alias']);

        $request = \TestHelper::createRequest('http://example.loc/blog/news/subnews');
        $result = $manager->handleRequest($request);
        $this->assertEmpty($result);
    }

    public function testCreateUrls()
    {
        $manager = new Manager();
        $manager->addRules([
            '/blog' => 'blog/index',
            '/blog/{id:any}' => 'blog/view',
            '/blog/{alias:s}/{id:d}' => 'blog/category/view',
        ]);

        $this->assertEquals('/blog/10',$manager->createUrl('blog/view', ['id' => 10]));
        $this->assertFalse($manager->createUrl('blog/view', ['id' => 10, 'alias' => 'value']));
        $this->assertFalse($manager->createUrl('blog/view'));
        $this->assertEquals('/blog', $manager->createUrl('blog/index'));
        $this->assertEquals('/blog/news/10', $manager->createUrl('blog/category/view', ['alias' => 'news', 'id' => 10]));

        $manager->setRequest(TestHelper::createRequest('http://example.loc/blog/10'));
        $this->assertEquals('http://example.loc/blog/10',$manager->createAbsoluteUrl('blog/view', ['id' => 10]));
    }
}

