<?php
return [
    'core\base\ErrorHandler' => CORE_DIR.'/ErrorHandler.php',
    'core\base\UnknownClassException' => CORE_DIR.'/UnknownClassException.php',
    'core\base\ActionNotFound' => CORE_DIR.'/ActionNotFound.php',
    'core\base\NotFoundLibrary' => CORE_DIR.'/NotFoundLibrary.php',
    'core\base\routing\Request' => CORE_DIR.'/routing/Request.php',
    'core\base\routing\Manager' => CORE_DIR.'/routing/Manager.php',
    'core\base\routing\Rule' => CORE_DIR.'/routing/Rule.php',
    'core\base\Application' => CORE_DIR.'/Application.php',
    'core\base\Component' => CORE_DIR.'/Component.php',
    'core\base\Controller' => CORE_DIR.'/Controller.php',


];