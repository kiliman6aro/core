<?php

use core\base\InvalidParamException;
use core\base\UnknownClassException;

define('CORE_DIR', __DIR__.'/base');

class Core {
    /**
     * Список уже зарегестрированных namespaces
     * @var array
     */
    public static $classesMap = [];


    /**
     * @var \core\base\Application
     */
    public static $app;

    public static $aliases = ['@core' => __DIR__];


    public static function autoload($className)
    {
        if(isset(static::$classesMap[$className])){
           $classFile = static::$classesMap[$className];
        }else{
            $classFile = self::getPathOfAlias('@'.str_replace('\\', '/', $className). '.php', false);
            if($classFile == false || !is_file($classFile)){
                return;
            }
        }
        include($classFile);
        if (!class_exists($className, false) && !interface_exists($className, false) && !trait_exists($className, false)) {
            throw new UnknownClassException("Unable to find '$className' in file: $classFile. Namespace missing?");
        }
    }

    public static function configure($object, $params)
    {
        foreach($params as $param => $value){
            $object->$param = $value;
        }
        return $object;
    }

    /**
     * Возвращает путь к файлу при помощи alias. Если
     * в пути с алиасом присутствуют слешу, то обрезаю
     * по первому вхождению, ищу получаю путь до него
     * и конкотенирую с отавшуюся часть
     * @param $alias
     * @param $throwException bool бросать исключение или возвращать false в случае ошибки
     * @return string | bool
     * @throws InvalidParamException
     */
    public static function getPathOfAlias($alias, $throwException = true)
    {
        $pos = strpos($alias, '/');
        $root = $pos === false ? $alias : substr($alias, 0, $pos);

        if(isset(self::$aliases[$root])){
            return $pos == false ? self::$aliases[$root] : self::$aliases[$root] . substr($alias, $pos);
        }
        if($throwException){
            throw new InvalidParamException(sprintf("Alias %s not found", $alias));
        }
        return false;
    }

    /**
     * Устанавливает новый alias
     * @param $alias
     * @param $path
     */
    public static function setAlias($alias, $path)
    {
        self::$aliases[$alias] = $path;
    }
}
spl_autoload_register(['Core', 'autoload']);
Core::$classesMap = require __DIR__ . '/classes.php';