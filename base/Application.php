<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace core\base;


use core\base\routing\Request;

class Application
{
    public $defaultRoute = 'site/index';

    public $controllerNamespace = '\app\controllers';

    public $charset = 'utf8';


    private $_components = [];


    public function __construct($config = [])
    {
        $this->configure($config);
    }

    public function run()
    {
        \Core::$app = $this;
        $this->handleRequest($this->getRequest());
    }

    public function handleRequest(Request $request)
    {
        $result = $this->getUrlManager()->handleRequest($request);
        list($route, $params) = $result;
        $this->runAction($route, $params);
    }
    /**
     * Запускает контроллер, передавая ему параметры
     * @param $route <controller>/<action>
     * @param $params [] параметры
     */
    public function runAction($route, $params)
    {

        $config = $this->createController($route);
        if(is_array($config)){
            list($controller, $action) = $config;
            $controller->runAction($action, $params);
        }
    }

    /**
     * Создает новый контроллер на базе переданного route
     * @param $route
     * @return array|bool|null
     */
    public function createController($route)
    {
        if($route == '')
            $route = $this->defaultRoute;


        $route = trim($route, '/');

        if (strpos($route, '//') !== false) {
            return false;
        }

        if (strpos($route, '/') !== false) {
            list ($id, $route) = explode('/', $route, 2);
        } else {
            $id = $route;
            $route = '';
        }
        $className = str_replace(' ', '', ucwords($id)) . 'Controller';
        $className = ltrim($this->controllerNamespace . '\\' . str_replace('/', '\\', '')  . $className, '\\');
        $controller = new $className();
        $controller->id = $id;

        return get_class($controller) === $className ? [$controller, $route] : null;
    }

    /**
     * @return \core\base\routing\Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return \core\base\routing\Manager
     */
    public function getUrlManager()
    {
        return $this->urlManager;
    }


    public function __set($id, $value)
    {
        $this->_components[$id] = $value;
    }

    public function __get($id)
    {
        if(isset($this->_components[$id]))
            return $this->_components[$id];

        throw new NotFoundComponent(printf("Не найден компонент %s", $id));
    }

    /**
     * Задает компоненты для приложения извлекая их из массива
     * [
     *    'class' => '\app\components\ClassName',
     *    'param1' => 'value',
     *    'param2' => 'value2',
     *    ....
     * ]
     * @param array $components
     */
    public function setComponents($components = [])
    {
        foreach($components as $id => $params){
            if(!isset($params['class']) && isset($this->coreComponents()[$id])){
                $params['class'] = $this->coreComponents()[$id];
            }
            $class = $params['class'];
            $object = new $class();
            unset($params['class']);
            \Core::configure($object, $params);
            $this->_components[$id] = $object;
        }
    }

    /**
     * Компоненты, которые являются частью ядра
     * @return array
     */
    public function coreComponents()
    {
        return [
            'urlManager' => '\core\base\routing\Manager',
            'request' => '\core\base\routing\Request',
            'errorHandler' => '\core\base\ErrorHandler'
        ];
    }

    /**
     * Конфегурирует приложение, без необходимости создавать
     * новый экземляр класса
     * @param array $config
     */
    public function configure($config = [])
    {
        foreach($config['aliases'] as $alias => $path){
            \Core::setAlias($alias, $path);
        }
        $core = $this->coreComponents();
        foreach ($core as $id => $component) {
            if (!isset($config['components'][$id])) {
                $config['components'][$id]['class'] = $component;
            } elseif (is_array($config['components'][$id]) && !isset($config['components'][$id]['class'])) {
                $config['components'][$id]['class'] = $component;
            }
        }
        foreach($config['components'] as $id => $component){
            if(!isset($component['class'])){
                continue;
            }
            $class = $component['class'];
            $object = new $class();
            unset($component['class']);
            \Core::configure($object, $component);
            if($object instanceof Component){
                $object->init();
            }
            $this->_components[$id] = $object;
        }

        unset($config['components']);
        \Core::configure($this, $config);
    }
}