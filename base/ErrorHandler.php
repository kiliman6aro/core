<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace core\base;


class ErrorHandler extends Component
{

    public $errorView = '@core/views/error.php';

    public function init()
    {
        $this->register();
    }

    public function register()
    {
        ini_set('display_errors', false);
        set_exception_handler([$this, 'handleException']);
    }

    public function unregister()
    {
        restore_exception_handler();
    }

    public function handleException($exception)
    {
        $this->unregister();

        $this->renderFile($this->errorView, ['exception' => $exception]);
    }

    public function renderFile($file, $params)
    {
        extract($params);
        require(\Core::getPathOfAlias($file));
    }

}