<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace core\base;


class UnknownClassException extends \Exception{

    public function getName()
    {
        return 'Unknown class';
    }
}