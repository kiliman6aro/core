<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace core\base\routing;


use \core\base\Component;

class Manager extends Component{

    public $rules = [];

    protected $request;

    /**
     * @param Request|null $request
     */
    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    public function init()
    {
        $rules = $this->rules;
        $this->rules = [];
        $this->addRules($rules);
    }


    /**
     * Геннерируер массив объектов @see core\base\routing\Rule
     * и задает их в коллецию
     * @param \core\base\routing\Rule[] $rules
     */
    public function addRules($rules)
    {
        foreach($rules as $pattern => $route){
            $this->addRule($pattern, $route);
        }
    }

    /**
     * Генерирует новый экземпляр объекта \core\base\routing\Rule и
     * добавляет его в коллекцию
     * @param $pattern
     * @param $route
     */
    public function addRule($pattern, $route)
    {
        $this->rules[$route] = new Rule($pattern, $route);
    }

    /**
     * Обрабатывает запрос, проверяя сопостовление с правилами
     * в коллекции.
     * @param Request $request
     * @return bool | array
     */
    public function handleRequest(Request $request)
    {
        foreach ($this->rules as $rule) {
            if($result = $rule->parseRequest($request)){
                return $result;
            }
        }
        return false;
    }

    /**
     * Устанавливает новый экземпляр объекта
     * \core\base\routing\Request
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Генерирует относительный URL, при помоще правила
     * @param $route controller/action
     * @param array $params параметры
     * @return string
     */
    public function createUrl($route, $params = [])
    {
        if(!isset($this->rules[$route]))
            return '/';

        return $this->rules[$route]->createUrl($params);
    }

    /**
     * Делает тоже самое что и @see self::createUrl
     * но добавляет хост и делет url абсолютным
     * @param $route
     * @param array $params
     * @return string
     */
    public function createAbsoluteUrl($route, $params = [])
    {
        $url = $this->createUrl($route, $params);
        if($this->request){
            return $this->request->getHttpHost().$url;
        }
        return $url;
    }

}