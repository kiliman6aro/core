<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace core\base\routing;


class Request {

    protected $data;

    protected $pathInfo;

    public function __construct($data = null)
    {
        $this->data = $data != null ? $data : $_SERVER;
    }

    public function isGet()
    {
        return $this->getMethod() == 'GET';
    }

    public function isPost()
    {
        return $this->getMethod() == 'POST';
    }

    public function isAjax()
    {
        return isset($this->data['HTTP_X_REQUESTED_WITH']) && $this->data['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
    }

    /**
     * Проверяет является ли запрос защищенным HTTPS
     * @return bool
     */
    public function isSecure()
    {
        return isset($this->data['HTTPS']) && $this->data['HTTPS'] != 'off';
    }

    /**
     * Возвращает часть пути URL по которому
     * строится controller/action (до параметров)
     * @param null $baseUrl
     * @return string
     */
    public function getPathInfo($baseUrl = null)
    {
        if($this->pathInfo)
            return $this->pathInfo;

        $this->pathInfo = $this->data['REQUEST_URI'] ? $this->data['REQUEST_URI'] : '/';

        $url = $this->isSecure() ? 'https://' : 'http://';
        $url .= $this->getHost();

        if(strpos($this->pathInfo, $url) === 0){
            $this->pathInfo = substr($this->pathInfo, strlen($url));
        }

        if ($pos = strpos($this->pathInfo, '?')) {
            $this->pathInfo = substr($this->pathInfo, 0, $pos);
        }

        if (null != $baseUrl) {
            $this->pathInfo = substr($this->pathInfo, strlen($this->pathInfo));
        }

        //Удалить последний слеш, если он есть
        if (substr($this->pathInfo, strlen($this->pathInfo)-1) == "/"){
            $this->pathInfo = substr($this->pathInfo,0,strlen($this->pathInfo)-1);
        }

        return $this->pathInfo ? $this->pathInfo : '/';
    }

    /**
     * @return string путь файла сценария
     */
    public function getScriptFile()
    {
        return isset($this->_scriptFile) ? $this->_scriptFile : $this->data['SCRIPT_FILENAME'];
    }

    /**
     * Возвращает имя хоста, без http
     * @return mixed
     */
    public function getHost()
    {
        return $this->data['HTTP_HOST'];
    }

    /**
     * Возвращает имя хоста вместе с протоколом http:// | https://
     * @return string
     */
    public function getHttpHost()
    {
        $protocol = $this->isSecure() ? 'https://' : 'http://';

        return $protocol.$this->getHost();
    }

    /**
     * Строка запроса, список параметров все после ?
     * @return string
     */
    public function getQueryString()
    {
        return isset($this->data['QUERY_STRING']) ? $this->data['QUERY_STRING'] : '';
    }

    /**
     * Определяет какой метод запроса выполняется GET, POST, PUT etc
     * @return string
     */
    private function getMethod()
    {
        return isset($this->data['REQUEST_METHOD']) ? strtoupper($this->data['REQUEST_METHOD']) : 'GET';
    }
}