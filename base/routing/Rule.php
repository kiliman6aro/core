<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2016
 */

namespace core\base\routing;


class Rule {

    const REG_TPL_PARAM = '#\{(\w+):(\w+)\}#';


    /**
     * @var string шаблон правила, в челокочитаемом виде
     */
    public $pattern;

    /**
     * @var string направление <controller><action>
     */
    public $route;

    /**
     * @var mixed регулярное вырожение сгенерированное на базе шаблона
     */
    public $regular;

    /**
     * Соответствие шаблона к регулярному вырожению
     * @var array
     */
    protected $types = [
        'd' => '[0-9]+',
        's' => '[a-zA-Z\.\-_%]+',
        'any' => '[a-zA-Z0-9\.\-_%]+',
    ];

    /**
     * Генерирует регулярное выражение, по заданному правилу
     * @param $pattern string шаблон правила
     * @param $route string направление <controller>/<action>
     */
    public function __construct($pattern, $route)
    {
        $this->pattern = $pattern;
        $this->route = $route;

        if(!preg_match(self::REG_TPL_PARAM, $pattern)){
            return;
        }
        $this->regular = preg_replace_callback(self::REG_TPL_PARAM, function($match){
            $name = $match[1];
            $pattern = $match[2];

            return '(?<' . $name . '>' . strtr($pattern, $this->types) . ')';
        }, $pattern);
    }

    /**
     * Парсит текущий запрос, на route и параметры.
     * Если данное правило не совпадает, возвращает false
     * @param Request $request
     * @return array|bool
     */
    public function parseRequest(Request $request)
    {
        if($this->pattern == $request->getPathInfo()){
            return [$this->route, []];
        }

        if($this->regular && preg_match('#^'.$this->regular.'$#s', $request->getPathInfo(), $parameters)){
            foreach ($parameters as $k => $v) {
                if (is_int($k)) {
                    unset($parameters[$k]);
                }
            }
            return [$this->route, $parameters];
        }
        return false;
    }

    /**
     * Генерирует URL на базе переданных параметров и возвращает URL
     * @param array $params
     * @return string
     */
    public function createUrl($params = [])
    {
        $rParams = [];
        foreach($params as $k => $v){
            $name = '(:' . $k . ')';
            $rParams[$name] = $v;
        }
        $map = preg_replace(self::REG_TPL_PARAM, '(:$1)', $this->pattern, -1, $count);

        //Если количество переданных параметров не совпадает с количеством alias
        if(count($rParams) != $count)
            return false;

        return strtr($map, $rParams);
    }
}